#!/usr/bin/env python3

import sys
from urllib import parse
from xml.etree import ElementTree as etree

import requests
import tidylib

REQUEST_TIMEOUT = 5 * 60  # seconds
RETRIES = 3

# URLs to skip.
extracted_urls = {
    'https://feeds.feedburner.com',
}


def urlopen(url):
    for i in range(RETRIES):
        try:
            response = requests.get(
                url,
                headers={
                    'User-agent': 'archiveorg-backup',
                },
                timeout=REQUEST_TIMEOUT,
            )
            response.raise_for_status()
            return response
        except Exception as error:
            if i == RETRIES - 1:
                raise error
            else:
                print("WARNING: Exception getting '{url}': {error}".format(url=url, error=error), file=sys.stderr)


def print_url(url):
    global extracted_urls

    url = url.strip()
    url = parse.urlunparse(parse.urlparse(url)._replace(fragment=""))

    if url not in extracted_urls:
        extracted_urls.add(url)
        print(url)


def process_feed(url):
    failed = 0

    try:
        base_url = parse.urlunparse(parse.urlparse(url)._replace(path="", params="", query="", fragment=""))
        print_url(base_url)

        with urlopen(url) as feed_response:
            tidy_xml = tidylib.tidy_document(feed_response.text, {'input_xml': True})[0]
            tree = etree.fromstring(tidy_xml)

            for link in tree.findall('./channel/item/link'):
                print_url(link.text)

    except KeyboardInterrupt:
        raise
    except Exception as error:
        print("ERROR: Exception processing feed '{feed}': {error}".format(feed=url, error=error), file=sys.stderr)
        failed += 1

    return failed


def main(argv):
    failed = 0

    for url in argv[1:]:
        if not url:
            continue

        failed += process_feed(url)

    return failed


if __name__ == '__main__':
    failed = main(sys.argv)
    sys.exit(bool(failed))
